const local = document.querySelector(".local")
var localScript = script = document.createElement('script');
const random = document.querySelector(".random")
var randomScript = document.createElement('script');
const noob = document.querySelector(".noob")
var noobScript = document.createElement('script');
const smart = document.querySelector(".smart")
var smartScript = document.createElement('script');
const clean = document.querySelector(".clean")

local.addEventListener('click', event => {
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', '2player.js');
    document.body.appendChild(script);
})
random.addEventListener('click', event => {
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', 'bot.js');
    document.body.appendChild(script);
})
noob.addEventListener('click', event => {
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', 'botmiddle.js');
    document.body.appendChild(script);
})
smart.addEventListener('click', event => {
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', 'bothard.js');
    document.body.appendChild(script);
})

clean.addEventListener('click', event => {
    location.reload();
})