let gridGame = document.getElementById('grid-game')
let sectionWin = document.getElementById('win')

document.onload=gridCreate()

function gridCreate (){
    for(let n = 0 ; n < 42 ; n++){
        const newDiv = document.createElement("div");
        newDiv.dataset.numero = n
        newDiv.dataset.col = 'col-'+n%7
        newDiv.dataset.taken = 'no'
    gridGame.appendChild(newDiv);
    }
}
var cells = document.querySelectorAll("[data-numero]")
var cellsTaken = document.querySelectorAll("[data-taken='yes']")
var cellsNotTaken = document.querySelectorAll("[data-taken='no']")
var player = "player"
var bot = "bot"
var actualPlayer = player
var playerCells = document.querySelectorAll('.player')
var botCells = document.querySelectorAll('.bot')
var player1;
var player2;
Name()

cells.forEach(cell => {
    cell.addEventListener('click', event => {
        if (document.querySelector('.win')){
            return;
            }
        else {
        var columnCell = cell.dataset.col
        var lastCellColumn = document.querySelectorAll("[data-col='"+columnCell+"'][data-taken='no']")
        var lastCell = lastCellColumn.item(lastCellColumn.length-1)
            if (actualPlayer === player){
                actualMove(actualPlayer, lastCell)
                checkWin()
                actualPlayer = bot
                if (document.querySelector('.win')){
                    return;
                }
            }
            else{
                actualMove(actualPlayer, lastCell)
                checkWin()
                actualPlayer = player
            }
        }
        cellsTaken = document.querySelectorAll("[data-taken='yes']")
        cellsNotTaken = document.querySelectorAll("[data-taken='no']")
    })
})


function actualMove(actualPlayer, lastObject){
    lastObject.classList.add(actualPlayer)
    lastObject.dataset.taken = 'yes' 
}

function checkWin(){
    winningArrays.forEach(winningArray => {
        let counter = 0
        winningArray.forEach(number => {
            if(document.querySelector('[data-numero="'+number+'"]').classList.contains(actualPlayer)){
                counter++
            }
        });
        if (counter === 4){
            if (actualPlayer === player){
                const win = document.createElement("h2");
                win.classList.add('win')
                let winText = document.createTextNode (player1+' Wins');
                win.appendChild(winText)
                sectionWin.appendChild(win);
                sectionWin.classList.replace("height0", "height80")
            }
            else {
                const win = document.createElement("h2");
                win.classList.add('win')
                let winText = document.createTextNode (player2+' Wins');
                win.appendChild(winText)
                sectionWin.appendChild(win);
                sectionWin.classList.replace("height0", "height80")
            }
        }
    })
}

function Name() {
    player1 = prompt('Player 1, choose a username');
    while(player1 === ''){
        player1 = prompt("Username unvalid choose another one");
    }
    player2 = prompt('Player 2, choose a username');
    while (player1 === player2 || player2 === ''){
        player2 = prompt("Username unvalid choose another one");
    }
}