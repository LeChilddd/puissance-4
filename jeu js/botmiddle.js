var gridGame = document.getElementById('grid-game')
let sectionWin = document.getElementById('win')

document.onload=gridCreate()
function gridCreate (){
    for(let n = 0 ; n < 42 ; n++){
        const newDiv = document.createElement("div");
        newDiv.dataset.numero = n
        newDiv.dataset.col = 'col-'+n%7
        newDiv.dataset.taken = 'no'
    gridGame.appendChild(newDiv);
    }
}
var cells = document.querySelectorAll("[data-numero]")
var cellsTaken = document.querySelectorAll("[data-taken='yes']")
var cellsNotTaken = document.querySelectorAll("[data-taken='no']")
var player = "player"
var bot = "bot"
var actualPlayer = player
var playerCells = document.querySelectorAll('.player')
var botCells = document.querySelectorAll('.bot')

Name();

cells.forEach(cell => {
    cell.addEventListener('click', event => {
        if (document.querySelector('.win')){
            return
        }
        else {
            var columnCell = cell.dataset.col
            var lastCellColumn = document.querySelectorAll("[data-col='"+columnCell+"'][data-taken='no']")
            var lastCell = lastCellColumn.item(lastCellColumn.length-1)
            playerMove(actualPlayer, lastCell)
            checkWin()
            if (document.querySelector('.win')){
                return
            }
            else {
                actualPlayer = bot
                botMove(actualPlayer, lastCell)
                checkWin()
                actualPlayer = player
                cellsTaken = document.querySelectorAll("[data-taken='yes']")
                cellsNotTaken = document.querySelectorAll("[data-taken='no']")
            }
        }
    })
})


function playerMove(actualPlayer, lastObject){
    lastObject.classList.add(actualPlayer)
    lastObject.dataset.taken = 'yes' 
}
function botMove(actualPlayer, columnCell){
    var move = getRandomInt(6)
    if (move < 8){
        for(let i = 0; i < horizontalwinningArrays.length; i++) {
            var horizontalwinningArray = horizontalwinningArrays[i];
            var horizontalCounter = 0
            var horizontalEmptyCellIndex = 0
            for (let j = 0; j < horizontalwinningArray.length; j++){

                if(document.querySelector('[data-numero="'+horizontalwinningArray[j]+'"]').classList.contains(player)){
                    horizontalCounter++
                    if (horizontalCounter === 2){
                        if(horizontalwinningArray.forEach(element => {
                            document.querySelector('[data-numero="'+element+'"]').classList.contains(actualPlayer)
                        })) {
                            checkWin()
                        }
                        else{
                            let criticalCellColumnData = document.querySelector('[data-numero="'+horizontalwinningArray[horizontalEmptyCellIndex]+'"]').dataset.col
                            let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                            let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                            console.log(botLastcriticalCellColumn)
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            horizontalwinningArrays.splice(i, 1)
                            horizontalCounter = 0
                            return;
                        }
                    }
                }
                else {
                    horizontalCounter = 0
                    horizontalEmptyCellIndex = j
                }
            }
        }
        for(let i = 0; i < verticalwinningArrays.length; i++) {
            var verticalwinningArray = verticalwinningArrays[i];
            var verticalCounter = 0
            var verticalEmptyCellIndex = 0
            for (let j = 0; j < verticalwinningArray.length; j++){

                if(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]').classList.contains(player)){
                    verticalCounter++
                    if (verticalCounter >= 3){
                        console.log(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]'))
                        console.log(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]'))
                        if(verticalwinningArray.forEach(element => {
                            document.querySelector('[data-numero="'+element+'"]').classList.contains(actualPlayer)
                        })){
                            checkWin()
                        }
                        else if (j+1 < verticalwinningArray.length && (document.querySelector('[data-numero="'+(verticalwinningArray[j+1])+'"]').classList.contains(actualPlayer) || columnCell.length === 0)){
                            verticalwinningArrays.splice(i, 1)
                        }
                        else{
                            let criticalCellColumnData = document.querySelector('[data-numero="'+verticalwinningArray[verticalEmptyCellIndex]+'"]').dataset.col
                            let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                            let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            verticalwinningArrays.splice(i, 1)
                            verticalCounter = 0
                            return;
                        }
                    }
                }
                else {
                    verticalCounter = 0
                    verticalEmptyCellIndex = j
                }
            }
        }
        for(let i = 0; i < diagonalwinningArrays.length; i++) {
            var diagonalwinningArray = diagonalwinningArrays[i];
            var diagonalCounter = 0
            var diagonalEmptyCellIndex = 0
            for (let j = 0; j < diagonalwinningArray.length; j++){

                if(document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').classList.contains(player)){
                    diagonalCounter++
                    if (diagonalCounter === 3){
                       console.log(diagonalCounter)
                        if (document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').classList.contains(bot)){
                            diagonalwinningArrays.splice(i, 1)
                        }
                        else{
                            let criticalCellColumnData = document.querySelector('[data-numero="'+diagonalwinningArray[diagonalEmptyCellIndex]+'"]').dataset.col
                            let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                            console.log(criticalCellColumn)
                            let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                            if (botLastcriticalCellColumn){
                                console.log(botLastcriticalCellColumn)
                                botLastcriticalCellColumn.classList.add(actualPlayer)
                                botLastcriticalCellColumn.dataset.taken = 'yes'
                                diagonalCounter = 0
                                return;
                            }
                            else {
                                diagonalCounter = 0
                                continue;
                            }
                        }
                    }
                }
                else {
                    diagonalCounter = 0
                    diagonalEmptyCellIndex = j
                }
            }
        }
        botMiddleMove() 
    }
    else {
        console.log('zozo')
        botMiddleMove() 
    }
}

function checkWin(){
    horizontalCheckWin()
    verticalCheckWin()
    diagonalCheckWin()
}


function horizontalCheckWin(){
    let counter = 0
    let counterdel = 0
    console.log('horizontal'+horizontalwinningArrays.length)
    for (let i = 0; i < horizontalwinningArrays.length; i++){
        var horizontalwinningArray = horizontalwinningArrays[i]
        for (let j = 0; j < horizontalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+horizontalwinningArray[j]+'"]').classList.contains(actualPlayer)){
                counter++
                if(counter === 4){
                    win(actualPlayer)
                }
            }
            else if(document.querySelector('[data-numero="'+horizontalwinningArray[j]+'"]').dataset.taken === 'yes'){
                counterdel++
                if(counterdel === 4){
                    horizontalwinningArrays.splice(i, 1)
                }
            }
            else{
                counter = 0
                counterdel = 0
            }
        }
        counter = 0
    }
}
function verticalCheckWin(){
    let counter = 0
    let counterdel =0
    console.log('vertical'+verticalwinningArrays.length)
    for (let i = 0; i < verticalwinningArrays.length; i++){
        var verticalwinningArray = verticalwinningArrays[i]
        for (let j = 0; j < verticalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]').classList.contains(actualPlayer)){
                counter++
                if(counter === 4){
                    console.log(actualPlayer)
                    win(actualPlayer)
                }
            }
            else if(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]').dataset.taken === 'yes'){
                counterdel++
                if(counterdel === 4){
                    verticalwinningArrays.splice(i, 1)
                }
            }
            else{
                counter = 0
                counterdel = 0
            }
        }
        counter = 0
    }
}

function diagonalCheckWin(){
    console.log('diagonal'+diagonalwinningArrays.length)
    let counter = 0
    let counterdel = 0
    for (let i = 0; i < diagonalwinningArrays.length; i ++){
        var diagonalwinningArray = diagonalwinningArrays[i]
        for (let j = 0; j < diagonalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').classList.contains(actualPlayer)){
                counter++
                if(counter === 4){
                    win(actualPlayer)
                }
                else if(document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').dataset.taken === 'yes'){
                    counterdel++
                    if(counterdel === 4){
                        diagonalwinningArrays.splice(i, 1)
                    }
                }
            }
            else{
                counter = 0
                counterdel = 0
            }
        }
        counter = 0
    }
}


function botMiddleMove (){
    var botColumnNumber = getRandomInt(6)
        var botCellColumn = document.querySelectorAll("[data-col='col-"+botColumnNumber+"'][data-taken='no']")
        if (botCellColumn.length === 0){
            botMove(actualPlayer)
        }
        else {var botLastCellColumn = botCellColumn.item(botCellColumn.length-1)
        botLastCellColumn.classList.add(actualPlayer)
        botLastCellColumn.dataset.taken = 'yes'
        } 
}


function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function win(actualPlayer){
    if (actualPlayer === player){
        const win = document.createElement("h2");
        win.classList.add('win')
        let winText = document.createTextNode (player1+' Wins');
        win.appendChild(winText)
        sectionWin.appendChild(win);
        sectionWin.classList.replace("height0", "height80")
    }
    else{
    const win = document.createElement("h2");
    win.classList.add('win')
    let winText = document.createTextNode (actualPlayer+' Wins');
    win.appendChild(winText)
    sectionWin.appendChild(win);
    sectionWin.classList.replace("height0", "height80")
    }
}

function Name() {
    playerOne = prompt('Player 1, choose a username ?');
    while(playerOne === ''){
        playerOne = prompt("Player 1, invalid username choose another one");
    }
}