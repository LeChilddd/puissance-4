let gridGame = document.getElementById('grid-game')

const winningArrays = [
    [0, 1, 2, 3],
    [41, 40, 39, 38],
    [7, 8, 9, 10],
    [34, 33, 32, 31],
    [14, 15, 16, 17],
    [27, 26, 25, 24],
    [21, 22, 23, 24],
    [20, 19, 18, 17],
    [28, 29, 30, 31],
    [13, 12, 11, 10],
    [35, 36, 37, 38],
    [6, 5, 4, 3],
    [0, 7, 14, 21],
    [41, 34, 27, 20],
    [1, 8, 15, 22],
    [40, 33, 26, 19],
    [2, 9, 16, 23],
    [39, 32, 25, 18],
    [3, 10, 17, 24],
    [38, 31, 24, 17],
    [4, 11, 18, 25],
    [37, 30, 23, 16],
    [5, 12, 19, 26],
    [36, 29, 22, 15],
    [6, 13, 20, 27],
    [35, 28, 21, 14],
    [0, 8, 16, 24],
    [41, 33, 25, 17],
    [7, 15, 23, 31],
    [34, 26, 18, 10],
    [14, 22, 30, 38],
    [27, 19, 11, 3],
    [35, 29, 23, 17],
    [6, 12, 18, 24],
    [28, 22, 16, 10],
    [13, 19, 25, 31],
    [21, 15, 9, 3],
    [20, 26, 32, 38],
    [36, 30, 24, 18],
    [5, 11, 17, 23],
    [37, 31, 25, 19],
    [4, 10, 16, 22],
    [2, 10, 18, 26],
    [39, 31, 23, 15],
    [1, 9, 17, 25],
    [40, 32, 24, 16],
    [9, 17, 25, 33],
    [8, 16, 24, 32],
    [11, 17, 23, 29],
    [12, 18, 24, 30],
    [1, 2, 3, 4],
    [5, 4, 3, 2],
    [8, 9, 10, 11],
    [12, 11, 10, 9],
    [15, 16, 17, 18],
    [19, 18, 17, 16],
    [22, 23, 24, 25],
    [26, 25, 24, 23],
    [29, 30, 31, 32],
    [33, 32, 31, 30],
    [36, 37, 38, 39],
    [40, 39, 38, 37],
    [7, 14, 21, 28],
    [8, 15, 22, 29],
    [9, 16, 23, 30],
    [10, 17, 24, 31],
    [11, 18, 25, 32],
    [12, 19, 26, 33],
    [13, 20, 27, 34],
  ]

document.onload=gridCreate()
function gridCreate (){
    for(let r = 1; r < 7; r++){
        for(let c = 1; c < 8; c++){
            const newDiv = document.createElement("div");
            newDiv.dataset.col = 'col-'+c
            newDiv.dataset.row = 'row-'+r
            newDiv.dataset.taken = 'no'
        gridGame.appendChild(newDiv);
        }
    }
}
var column = {
    c1: document.querySelectorAll("[data-col='col-1']"),
    c2: document.querySelectorAll("[data-col='col-2']"),
    c3: document.querySelectorAll("[data-col='col-3']"),
    c4: document.querySelectorAll("[data-col='col-4']"),
    c5: document.querySelectorAll("[data-col='col-5']"),
    c6: document.querySelectorAll("[data-col='col-6']"),
    c7: document.querySelectorAll("[data-col='col-7']"),
}

var player = "player"
var bot = "bot"
var actualPlayer = player


var playerObject = document.querySelectorAll('.player')
var botObject = document.querySelectorAll('.bot')

for(let i = 1; i <= Object.keys(column).length; i++){
    for (let j = 0; j < column["c"+i].length; j++){
        column["c"+i][j].addEventListener('click', event => {
            var columnTaken = {
                c1: document.querySelectorAll("[data-col='col-1'][data-taken='yes']"),
                c2: document.querySelectorAll("[data-col='col-2'][data-taken='yes']"),
                c3: document.querySelectorAll("[data-col='col-3'][data-taken='yes']"),
                c4: document.querySelectorAll("[data-col='col-4'][data-taken='yes']"),
                c5: document.querySelectorAll("[data-col='col-5'][data-taken='yes']"),
                c6: document.querySelectorAll("[data-col='col-6'][data-taken='yes']"),
                c7: document.querySelectorAll("[data-col='col-7'][data-taken='yes']"),
            }
            var columnNotTaken = {
                c1: document.querySelectorAll("[data-col='col-1'][data-taken='no']"),
                c2: document.querySelectorAll("[data-col='col-2'][data-taken='no']"),
                c3: document.querySelectorAll("[data-col='col-3'][data-taken='no']"),
                c4: document.querySelectorAll("[data-col='col-4'][data-taken='no']"),
                c5: document.querySelectorAll("[data-col='col-5'][data-taken='no']"),
                c6: document.querySelectorAll("[data-col='col-6'][data-taken='no']"),
                c7: document.querySelectorAll("[data-col='col-7'][data-taken='no']"),
            }
            var columnObjet = columnNotTaken["c"+i]
            var lastObject = columnObjet.item(columnObjet.length-1)
            if (actualPlayer === player){
                actualMove(actualPlayer, lastObject)
                actualPlayer = bot
            }
            else{
                actualMove(actualPlayer, lastObject)
                actualPlayer = player
            }
            playerObject = document.querySelectorAll('.player')
            botObject = document.querySelectorAll('.bot')

        })
    }
}

function actualMove(actualPlayer, lastObject){
    lastObject.classList.add(actualPlayer)
    lastObject.dataset.taken = 'yes' 
    var objectRow = lastObject.getAttribute('data-row')
    var objectCol = lastObject.getAttribute('data-col')
    var row = Array.from(document.querySelectorAll("[data-row="+objectRow+"]"))
    var col = Array.from(document.querySelectorAll("[data-col="+objectCol+"]"))
    var colCount = 0
    var rowCount = 0
    var diagonalCount = 0
    col.forEach(el => {
        if(el.classList.contains(actualPlayer)){
            colCount++
            if(colCount === 4){
                alert(actualPlayer+' win')
            }
        }
        else{
            colCount = 0
        }
    });
    row.forEach(el => {
        if(el.classList.contains(actualPlayer)){
            rowCount++
            if(rowCount === 4){
                alert(actualPlayer+'win')
            }
        }
        else{
            rowCount = 0
        }
    });
    var objectRowNum = objectRow.split('-')[1]
    var objectColNum = objectCol.split('-')[1]
}






    //              VERIF DIAGONALES
    // if (objectRowNum >= 3 && objectColNum >= 3){
    //     for (let t = 0; t < 4; t++){
    //         var object = document.querySelector("[data-row=row-"+(objectRowNum-t)+"][data-col=col-"+(objectColNum-t)+"]")
    //         console.log(object)
    //         if(object.classList.contains(actualPlayer)){
    //             diagonalCount++
    //             console.log('diagonalCount', diagonalCount)
    //             if(diagonalCount === 4){
    //                 alert(actualPlayer+' win')
    //             }
    //         }
    //         else{
    //             diagonalCount = 0
    //             console.log('diagonalCount', diagonalCount)
    //         }
    //     }
    // }
    // else if (objectRowNum >= 2 && objectColNum >= 2){
    //     for (let t = -1 ; t < 3; t++){
    //         var object = document.querySelector("[data-row=row-"+(objectRowNum-t)+"][data-col=col-"+(objectColNum-t)+"]")
    //         console.log(object)
    //         if(object.classList.contains(actualPlayer)){
    //             diagonalCount++
    //             console.log('diagonalCount', diagonalCount)
    //             if(diagonalCount === 4){
    //                 alert(actualPlayer+' win')
    //             }
    //         }
    //         else{
    //             diagonalCount = 0
    //             console.log('diagonalCount', diagonalCount)
    //         }
    //     }
    // }