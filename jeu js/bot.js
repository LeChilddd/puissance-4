let gridGame = document.getElementById('grid-game')
let sectionWin = document.getElementById('win')


document.onload=gridCreate()
function gridCreate (){
    for(let n = 0 ; n < 42 ; n++){
        const newDiv = document.createElement("div");
        newDiv.dataset.numero = n
        newDiv.dataset.col = 'col-'+n%7
        newDiv.dataset.taken = 'no'
    gridGame.appendChild(newDiv);
    }
}

Name()
var cells = document.querySelectorAll("[data-numero]")
var cellsTaken = document.querySelectorAll("[data-taken='yes']")
var cellsNotTaken = document.querySelectorAll("[data-taken='no']")
var player = "player"
var bot = "bot"
var actualPlayer = player
var playerCells = document.querySelectorAll('.player')
var botCells = document.querySelectorAll('.bot')
var playerOne;


cells.forEach(cell => {
    cell.addEventListener('click', event => {
        if (document.querySelector('.win')){
            console.log('toto')
            return;
        }
        else {
            var columnCell = cell.dataset.col
            var lastCellColumn = document.querySelectorAll("[data-col='"+columnCell+"'][data-taken='no']")
            var lastCell = lastCellColumn.item(lastCellColumn.length-1)
            playerMove(actualPlayer, lastCell)
            checkWin()
            if (document.querySelector('.win')){
                return;
            }
                else {
                actualPlayer = bot
                botMove(actualPlayer, lastCell)
                checkWin()
                actualPlayer = player
                cellsTaken = document.querySelectorAll("[data-taken='yes']")
                cellsNotTaken = document.querySelectorAll("[data-taken='no']")
            }
        }
    })
})


function playerMove(actualPlayer, lastObject){
    lastObject.classList.add(actualPlayer)
    lastObject.dataset.taken = 'yes' 
}
function botMove(actualPlayer){
    var botColumnNumber = getRandomInt(6)
    var botCellColumn = document.querySelectorAll("[data-col='col-"+botColumnNumber+"'][data-taken='no']")
    if (botCellColumn.length === 0){
        botMove(actualPlayer)
    }
    var botLastCellColumn = botCellColumn.item(botCellColumn.length-1)
    botLastCellColumn.classList.add(actualPlayer)
    botLastCellColumn.dataset.taken = 'yes' 
}

function checkWin(){
    winningArrays.forEach(winningArray => {
        let counter = 0
        winningArray.forEach(number => {
            if(document.querySelector('[data-numero="'+number+'"]').classList.contains(actualPlayer)){
                counter++
            }
        });
        if (counter === 4){
            if(actualPlayer === player){
                winMessage(playerOne)
            }
            else{
                winMessage(bot)
            }
        }
    })
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function Name() {
    playerOne = prompt('Player 1, choose a username ?');
    while(playerOne === ''){
        playerOne = prompt("Player 1, invalid username choose another one");
    }
}

function winMessage(actualPlayer){
    if (actualPlayer === player){
        const win = document.createElement("h2");
        win.classList.add('win')
        let winText = document.createTextNode (player1+' Wins');
        win.appendChild(winText)
        sectionWin.appendChild(win);
        sectionWin.classList.replace("height0", "height80")
    }
    else{
    const win = document.createElement("h2");
    win.classList.add('win')
    let winText = document.createTextNode (actualPlayer+' Wins');
    win.appendChild(winText)
    sectionWin.appendChild(win);
    sectionWin.classList.replace("height0", "height80")
    }
}