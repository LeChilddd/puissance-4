var gridGame = document.getElementById('grid-game')
let sectionWin = document.getElementById('win')


document.onload=gridCreate()
function gridCreate (){
    for(let n = 0 ; n < 42 ; n++){
        const newDiv = document.createElement("div");
        newDiv.dataset.numero = n
        newDiv.dataset.col = 'col-'+n%7
        newDiv.dataset.taken = 'no'
    gridGame.appendChild(newDiv);
    }
}
Name()
var cells = document.querySelectorAll("[data-numero]")
var cellsTaken = document.querySelectorAll("[data-taken='yes']")
var cellsNotTaken = document.querySelectorAll("[data-taken='no']")
var player = "player"
var bot = "bot"
var actualPlayer = player
var playerCells = document.querySelectorAll('.player')
var botCells = document.querySelectorAll('.bot')
var actualWinningArray;
var botMovetest;
var defenseArray = [];
var player1;


cells.forEach(cell => {
    cell.addEventListener('click', event => {
        if (document.querySelector('.win')){
            return;
            }
        else {
        var columnCell = cell.dataset.col
        var lastCellColumn = document.querySelectorAll("[data-col='"+columnCell+"'][data-taken='no']")
        var lastCell = lastCellColumn.item(lastCellColumn.length-1)
        playerMove(actualPlayer, lastCell)
        checkWins()
        actualPlayer = bot
            if (document.querySelector('.win')){
                return;
            }
            else {
                botMove(bot)
                checkWins()
                actualPlayer = player
                cellsTaken = document.querySelectorAll("[data-taken='yes']")
                cellsNotTaken = document.querySelectorAll("[data-taken='no']")
            }
        }
    })
})


function playerMove(actualPlayer, lastObject){
    lastObject.classList.add(actualPlayer)
    lastObject.dataset.taken = 'yes' 
}

function checkWins(){
    checkWin(actualPlayer, actualWinningArray, horizontalwinningArrays)
    checkWin(actualPlayer, actualWinningArray, verticalwinningArrays)
    checkWin(actualPlayer, actualWinningArray, diagonalwinningArrays)
}


function checkWin(actualPlayer, actualWinningArray, actualWinningArrays){
    let counter = 0
    for (let i = 0; i < actualWinningArrays.length; i++){
        var actualWinningArray = actualWinningArrays[i]
        for (let j = 0; j < actualWinningArray.length; j++){
            if(document.querySelector('[data-numero="'+actualWinningArray[j]+'"]').classList.contains(actualPlayer)){
                counter++
                if(counter === 4){
                    win(actualPlayer)
                    return;
                }
            }
            else{
                counter = 0
            }
        }
        counter = 0
    }
}

function botMiddleMove (){
    var botColumnNumber = getRandomInt(6)
        var botCellColumn = document.querySelectorAll("[data-col='col-"+botColumnNumber+"'][data-taken='no']")
        if (botCellColumn.length === 0){
            botMiddleMove()
        }
        else {
            var botLastCellColumn = botCellColumn.item(botCellColumn.length-1)
            botLastCellColumn.classList.add(actualPlayer)
            botLastCellColumn.dataset.taken = 'yes'
        } 
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function win(actualPlayer){
    if (actualPlayer === player){
        const win = document.createElement("h2");
        win.classList.add('win')
        let winText = document.createTextNode (player1+' Wins');
        win.appendChild(winText)
        sectionWin.appendChild(win);
        sectionWin.classList.replace("height0", "height80")
    }
    else{
    const win = document.createElement("h2");
    win.classList.add('win')
    let winText = document.createTextNode (actualPlayer+' Wins');
    win.appendChild(winText)
    sectionWin.appendChild(win);
    sectionWin.classList.replace("height0", "height80")
    }
}

function cleanWinningArray(actualWinningArrays){
    let counter = 0
    for (let i = 0; i < actualWinningArrays.length; i++){
    var actualWinningArray = actualWinningArrays[i]
        for (let j = 0; j < actualWinningArray.length; j++){
            if(document.querySelector('[data-numero="'+actualWinningArray[j]+'"]').classList.contains(player) || document.querySelector('[data-numero="'+actualWinningArray[j]+'"]').classList.contains(bot)){
                counter++
                if(counter === 4){
                    actualWinningArrays.splice(i, 1)
                }
            }
            else{
                counter = 0
            }
        }
    counter = 0
    }
}

function botMove(actualPlayer, columnCell){
    for(let i = 0; i < diagonalwinningArrays.length; i++) {
        var diagonalwinningArray = diagonalwinningArrays[i];
        var diagonalPlayerCounter = 0
        var diagonalBotCounter = 0
        var diagonalEmptyCellIndex = 0
        for (let j = 0; j < diagonalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').classList.contains(player)){
                diagonalPlayerCounter++
                if (diagonalPlayerCounter === 3 && diagonalBotCounter < 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+diagonalwinningArray[diagonalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    if (botLastcriticalCellColumn){
                        if (diagonalwinningArray[diagonalEmptyCellIndex]<35){
                            if(document.querySelector('[data-numero="'+(diagonalwinningArray[diagonalEmptyCellIndex]+7)+'"]').dataset.taken === 'yes'){
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            diagonalwinningArray.splice(i, 1)
                            diagonalPlayerCounter = 0
                            console.log('bloc diagonal')
                            return;
                            }
                            botMiddleMove()
                            return;
                        }
                        else{
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            diagonalwinningArrays.splice(i, 1)
                            diagonalPlayerCounter = 0
                            return;
                        }
                    }
                }
            }
            else if (document.querySelector('[data-numero="'+diagonalwinningArray[j]+'"]').classList.contains(bot)){
                diagonalBotCounter++
                if (diagonalBotCounter === 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+diagonalwinningArray[diagonalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    if (botLastcriticalCellColumn){
                        if (diagonalwinningArray[diagonalEmptyCellIndex]<35){
                            if(document.querySelector('[data-numero="'+(diagonalwinningArray[diagonalEmptyCellIndex]+7)+'"]').dataset.taken === 'yes'){
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            diagonalPlayerCounter = 0
                            console.log('win diagonal')
                            return;
                            }
                            botMiddleMove()
                            return;
                        }
                        else{
                            botLastcriticalCellColumn.classList.add(actualPlayer)
                            botLastcriticalCellColumn.dataset.taken = 'yes'
                            diagonalPlayerCounter = 0
                            return;
                        }
                    }
                }
            }
            else {
                diagonalPlayerCounter = 0
                diagonalBotCounter = 0
                diagonalEmptyCellIndex = j
            }
        }
    }
    
    for(let i = 0; i < verticalwinningArrays.length; i++) {
        var verticalwinningArray = verticalwinningArrays[i];
        var verticalBotCounter = 0
        var verticalPlayerCounter = 0
        var verticalEmptyCellIndex = 0
        for (let j = 0; j < verticalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]').classList.contains(player)){
                verticalPlayerCounter++
                if (verticalPlayerCounter >= 3 && verticalBotCounter < 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+verticalwinningArray[verticalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    botLastcriticalCellColumn.classList.add(actualPlayer)
                    botLastcriticalCellColumn.dataset.taken = 'yes'
                    console.log('bloc vertical')
                    verticalwinningArrays.splice(i, 1)
                    verticalPlayerCounter = 0
                    return;
                }
            }
            else if(document.querySelector('[data-numero="'+verticalwinningArray[j]+'"]').classList.contains(bot)){
                verticalBotCounter++
                if (verticalBotCounter === 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+verticalwinningArray[verticalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    if (botLastcriticalCellColumn)
                    botLastcriticalCellColumn.classList.add(actualPlayer)
                    botLastcriticalCellColumn.dataset.taken = 'yes'
                    console.log('win vertical')
                    verticalBotCounter = 0
                    return;
                }
                else {
                    botMiddleMove
                }
            }
            else {
                verticalPlayerCounter = 0
                verticalBotCounter = 0
                verticalEmptyCellIndex = j
            }
        }
    }
    
    
    for(let i = 0; i < horizontalwinningArrays.length; i++) {
        var horizontalwinningArray = horizontalwinningArrays[i];
        var horizontalPlayerCounter = 0
        var horizontalBotCounter = 0 
        var horizontalEmptyCellIndex = 0
        for (let j = 0; j < horizontalwinningArray.length; j++){
            if(document.querySelector('[data-numero="'+horizontalwinningArray[j]+'"]').classList.contains(player)){
                horizontalPlayerCounter++
                if (horizontalPlayerCounter <= 3 && horizontalBotCounter < 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+horizontalwinningArray[horizontalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    if (horizontalwinningArray[horizontalEmptyCellIndex]<35){
                        if(document.querySelector('[data-numero="'+(horizontalwinningArray[horizontalEmptyCellIndex]+7)+'"]').dataset.taken === 'yes'){
                        botLastcriticalCellColumn.classList.add(actualPlayer)
                        botLastcriticalCellColumn.dataset.taken = 'yes'
                        horizontalwinningArrays.splice(i, 1)
                        actualCounter = 0
                        console.log('bloc horizontal')
                        return;
                        }
                        console.log('porut')
                        botMiddleMove()
                        return;
                    }
                    else{
                        botLastcriticalCellColumn.classList.add(actualPlayer)
                        botLastcriticalCellColumn.dataset.taken = 'yes'
                        horizontalwinningArrays.splice(i, 1)
                        horizontalCounter = 0
                        return;
                    }
                }
            }
            else if(document.querySelector('[data-numero="'+horizontalwinningArray[j]+'"]').classList.contains(bot)){
                horizontalBotCounter++
                console.log(horizontalBotCounter)
                if (horizontalBotCounter === 3){
                    let criticalCellColumnData = document.querySelector('[data-numero="'+horizontalwinningArray[horizontalEmptyCellIndex]+'"]').dataset.col
                    let criticalCellColumn = document.querySelectorAll("[data-col='"+criticalCellColumnData+"'][data-taken='no']")
                    let botLastcriticalCellColumn = criticalCellColumn.item(criticalCellColumn.length-1)
                    if (horizontalwinningArray[horizontalEmptyCellIndex]<35){
                        if(document.querySelector('[data-numero="'+(horizontalwinningArray[(horizontalEmptyCellIndex)]+7)+"'][data-taken='yes']")){
                        botLastcriticalCellColumn.classList.add(actualPlayer)
                        botLastcriticalCellColumn.dataset.taken = 'yes'
                        horizontalwinningArrays.splice(i, 1)
                        actualCounter = 0
                        console.log('win horizontal')
                        return;
                        }
                        botMiddleMove()
                        return;
                    }
                    else{
                        botLastcriticalCellColumn.classList.add(actualPlayer)
                        botLastcriticalCellColumn.dataset.taken = 'yes'
                        horizontalwinningArrays.splice(i, 1)
                        horizontalBotCounter = 0
                        return;
                    }
                }
            }
            else {
                horizontalBotCounter = 0
                horizontalPlayerCounter = 0
                horizontalEmptyCellIndex = j
            }
        }
    }
    botMiddleMove()
    console.log('prout')
}
function Name() {
    player1 = prompt('Player 1, choose a username ?');
    while(player1 === ''){
        player1 = prompt("Player 1, invalid username choose another one");
    }
}